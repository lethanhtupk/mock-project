'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ProjectServers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      server_id: {
          type: Sequelize.INTEGER,
          references: {
            model: 'Servers',
            type: 'id'
          }
        },
      project_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Projects',
          type: 'id'
        }
      },
      usage: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ProjectServers');
  }
};