import EmployeeService from '../services/EmployeeService';
import Util from '../utils/Utils';

const utils = new Util();

class EmployeeController {
    
    static async getAllEmployees (req, res) {
        const {page, pageSize} = req.params;
        if(!Number(page) || !Number(pageSize)) {
            utils.setError(400, "Please input valid number");
            return utils.send(res);
        }
        try {
            const allServers = await EmployeeService.getAllEmployees(page, pageSize);
            if (allServers.length > 0) {
                utils.setSuccess(200, "Employee retrieved", allServers);
            } else {
                utils.setSuccess(204, "No employee found");
            }
            return utils.send(res);
        } catch (error) {
            utils.setError(404, error.message);
            return utils.send(res);
        }
    }

    static async getEmployeesBelongsTo (req, res) {
        const {projectId, page, pageSize} = req.params;
        if (!Number(projectId) || !Number(page) || !Number(pageSize)) {
            utils.setError(400, "Please input a valid project's id");
            return utils.send(res);
        }
        try {
            const employeeBelongsTo = await EmployeeService.getEmployeeBelongsTo(projectId, page, pageSize);
            if (!employeeBelongsTo) {
                utils.setError(404, "Cannot found the project");
                return utils.send(res);
            }
            if (employeeBelongsTo.length > 0) {
                utils.setSuccess(200, "Employee retrieved", employeeBelongsTo);
            }
            else {
                utils.setSuccess(204, "The project dont have employee rightnow");
            }
            return utils.send(res);
        } catch (error) {
            utils.setError(404, error.message);
            return utils.send(res);
        }
    }

    static async getEmployee (req, res) {
        const {id} = req.params;
        if (!Number(id)) {
            utils.setError(400, "Please input valid id");
            return utils.send(res);
        }
        try {
            const employee = await EmployeeService.getEmployee(id);
            if (!employee) {
                utils.setError(400, "Cannot found the employee");
            }
            else {
                utils.setSuccess(200, "Found employee", employee);
            }
            return utils.send(res);
        } catch (error) {
            utils.setError(404, error.message);
            return utils.send(res);
        }
    }

    static async createAndAddToProject (req, res) {
        const {projectId} = req.params;
        const role = req.body.role;
        if (!Number(projectId)) {
            utils.setError(400, "Please input valid project id");
            return utils.send(res);
        }
        if (!req.body.name || !req.body.title || !req.body.phone_number || !req.body.email || !role) {
            utils.setError(400, "Please provide completed information ");
            return utils.send(res);
        }
        delete req.body.role;
        const newEmployee = req.body;
        try {
            const employeeToCreate = await EmployeeService.addEmployee(newEmployee);
            if (!employeeToCreate) {
                utils.setError(400, "The employee with that name already exist");
                return utils.send(res);
            }
            else {
                const employee = await EmployeeService.addEmployeeToProject(employeeToCreate.id, projectId);
                if (!employee) {
                    utils.setError(404, "Cannot found the project with that projectId");
                }
                else {
                    utils.setSuccess(200, "Employee created and added to project");
                }
                return utils.send(res);
            }
        } catch (error) {
            utils.setError(404, error.message);
            return utils.send(res);
        }
    }

    static async createEmployee (req, res) {
        if (!req.body.name || !req.body.title || !req.body.phone_number || !req.body.email) {
            utils.setError(400, "Please provide completed information ");
            return utils.send(res);
        }
        const newEmployee = req.body;
        try {
            const employeeToCreate = await EmployeeService.addEmployee(newEmployee);
            if (!employeeToCreate) {
                utils.setError(400, "The employee with that name already exist");
            }
            else {
                utils.setSuccess(200, "Employee created", employeeToCreate);
            }
            return utils.send(res);
        } catch (error) {
            throw error;
        }
    }

    static async addEmployeeToProject (req, res) {
        const {employeeId, projectId} = req.params;
        const role = req.body.role;
        if (!Number(employeeId) || ! Number(projectId)) {
            utils.setError(400, "Please input valid id");
            return utils.send(res);
        }
        if (!role) {
            utils.setError(400, "Please complete input");
            return utils.send(res);
        }
        try {
            const employee = await EmployeeService.addEmployeeToProject(employeeId, projectId, role);
            if (!employee) {
                utils.setError(400, "Cannot found the employee or project or that employee already exist in project");
            } else {
                utils.setSuccess(200, "Employee added", employee);
            }
            return utils.send(res);
        } catch (error) {
            utils.setError(404, error.message);
            return utils.send(res);
        }
    }

    static async addMultiEmployeeToProject (req, res) {
        const {projectId} = req.params;
        if(!Number(projectId)) {
            utils.setError(400, "Please input valid project id");
            return utils.send(res);
        }
        if (!req.body.employeesId) {
            utils.setError(404, "Please complete information");
            return utils.send(res);
        }
        const employeesId = req.body.employeesId;
        try {
            for(let i =0; i< employeesId.length; i++) {
                const employee_project = await EmployeeService.addEmployeeToProject(employeesId[i], projectId);
                if (!employee_project) {
                    utils.setError(404, "Cannot found the employee or project or it already exist");
                    return utils.send(res);
                }
            }
            utils.setSuccess(200, "Employee added");
            return utils.send(res);
        } catch (error) {
            utils.setError(404, error.message);
            return utils.send(res);
        }
    }

    static async updateEmployee (req, res) {
        const {id} = req.params;
        if (!Number(id)) {
            utils.setError(400, "Please input valid id");
            return utils.send(res);
        }
        try {
            const alterEmployee = req.body;
            const updateEmployee = await EmployeeService.updateEmployee(id,alterEmployee);
            if (!updateEmployee) {
                utils.setError(400, "Cannot found the employee");
            }
            else {
                utils.setSuccess(200,"Employee updated", alterEmployee);
            }
            return utils.send(res);
        } catch (error) {
            utils.setError(404, error.message);
            return utils.send(res);
        }
    }

    static async deleteEmployee (req, res) {
        const {id} = req.params;
        try {
            const deleteEmployee = EmployeeService.deleteEmployee(id);
            if (!deleteEmployee) {
                utils.setError(400, "Cannot found the employee");
            }
            else {
                utils.setSuccess(200, "Employee deleted");
            }
            return utils.send(res);
        } catch (error) {
            utils.setError(400, error.message);
            return utils.send(res);
        }
    }

    static async removeEmployee(req, res) {
        const {employeeId, projectId} = req.params;
        if (!Number(employeeId) || !Number(projectId)) {
            utils.setError(400, "Please input vaild id");
            return utils.send(res);
        }
        try {
            const employeeProject = EmployeeService.removeEmployee(employeeId, projectId);
            if (!employeeProject) {
                utils.setError(404, "Cannot found the employee or project");
            }
            else {
                utils.setSuccess(200, "Employee removed");
            }
            return utils.send(res);
        } catch (error) {
            utils.setError(404, error.message)
            return utils.send(res);
        }
    }
}

export default EmployeeController;