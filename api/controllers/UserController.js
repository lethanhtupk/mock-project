import UserService from '../services/UserService';
import Util from '../utils/Utils';
import bcrypt from 'bcrypt';
import { totalmem } from 'os';
// import passport from 'passport';
// import LocalStrategy from 'passport-local';

const util = new Util();

class UserController {
    static async register(req, res) {
        // validate input from user
        if (!req.body.username || !req.body.password1 || !req.body.password2) {
            util.setError(400, "Please complete information");
            return util.send(res);
        }
        else if (req.body.password1 !== req.body.password2) {
            util.setError(400, "Please input match password");
            return util.send(res);
        }
        try {
            const newUser = await UserService.createUser({username: req.body.username, password: req.body.password1});
            if (!newUser) {
                util.setError(400, "Username already exists, please choose another username");
            }
            else {
                util.setSuccess(201, "User created");
            }     
            return util.send(res);
        } catch (error) {
            util.setError(404, error.message);
            return util.send(res);
        }
    }

    static async login (req, res) {
        if (!req.body.username || !req.body.password) {
            util.setError(400, "Please to completed information");
            return util.send(res);
        }
        try {
            const token = await UserService.loginUser(req.body);
            if (!token) {
                util.setError(400, "Your username or password was wrong, Please try again");
            } else {
                util.setSuccess(200, "User had loggin", token)
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error.message);
            return util.send(res);
        }
    }

    static async logout (req, res) {
        try {
            await UserService.logout(req.token.dataValues.token);
            util.setSuccess(200, "User was logout");
            return util.send(res);
        } catch (error) {
            util.setError(400, error.message);
            return util.send(res);
        }
    }

    static async logoutAll (req, res) {
        try {
            console.log(req.user);  
            await UserService.logoutAll(req.user);
            util.setSuccess(200, "User loggout all services")
            return util.send(res);
        } catch (error) {
            util.setError(400, error.message);
            return util.send(res);
        } 
    }
}

export default UserController;