import OrganizationService from '../services/OrganizationService';
import Util from '../utils/Utils';

const utils = new Util();

class OrganizationController {

    static async getAllOrganizations (req, res) {
        const {page, pageSize} = req.params;
        if (!Number(page) || !Number(pageSize)) {
            utils.setError(400, "Please input valid number");
            return utils.send(res);
        }
        try {
            const allOrganizations = await OrganizationService.getAllOrganizations(page, pageSize);
            if (allOrganizations.length > 0) {
                utils.setSuccess(200, "Organizations retrieved", allOrganizations);
            }
            else {
                utils.setSuccess(200, "Not found organization");
            }
            return utils.send(res);
        } catch (error) {
            utils.setError(404, error.message);
            return tils.send(res);
        }
    }

    static async getOrganization (req,res) {
        const {id} = req.params;
        if (!Number(id)) {
            utils.setError(400, "Please input valid value");
            return utils.send(res);
        }
        try {
            const organization = await OrganizationService.getOrganization(id);
            if (!organization) {
                utils.setError(400, "Not found organization");
            }
            else {
                utils.setSuccess(200, "Succees", organization);
            }
            utils.send(res);
        } catch (error) {
            utils.setError(404, error.message);
            return utils.send(res);
        }
    }

    static async addOrganization (req, res) {
        if (!req.body.name) {
            utils.setError(400, "Please completed information");
            return utils.send(res);
        }
        const newOrganization = req.body;
        try {
            const organizationToAdd = await OrganizationService.createOrganization(newOrganization);
            if (!organizationToAdd) {
                utils.setError(400, "The organization's name already exist");
            }
            else {
                utils.setSuccess(200, "Organization created", organizationToAdd);
            }
            return utils.send(res);
        } catch (error) {
            utils.setError(404, error.message);
            return utils.send(res);
        }
    }

    static async updateOrganization (req, res) {
        const {id} = req.params;
        if(!Number(id)) {
            utils.setError(400, "Please input valid value");
            return utils.send(res);
        }
        try {
            const alterOrganization = req.body;
            const updateServer = await OrganizationService.updateOrganization(id, alterOrganization);
            if (!updateServer) {
                utils.setError(400, "Cannot found the organization");
            }
            else {
                utils.setSuccess(200, "Organization updated", updateServer);
            }
            return utils.send(res);
        } catch (error) {
            utils.setError(404, error.message);
            return utils.send(res);
        }
    }

    static async deleteOrganization (req, res) {
        const {id} = req.params;
        if (!Number(id)) {
            utils.setError(400, "Please input valid value");
            return utils.send(res);
        }
        try {
            const deleteOrganization = await OrganizationService.deleteOrganization(id);
            if (!deleteOrganization) {
                utils.setError(400, "Cannot found the organization");
            }
            else {
                utils.setSuccess(200, "Organization deleted");
            }
            return utils.send(res);
        } catch (error) {
            utils.setError(404, error.message);
            return utils.send(res);
        }
    }
}

export default OrganizationController;