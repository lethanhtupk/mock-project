import ProjectService from '../services/ProjectService';
import Util from '../utils/Utils';

const utils = new Util();

class ProjectController {

    static async createProject (req, res) {
        if (!req.body.name || !req.body.organizationId) {
            utils.setError(400, "Please provide complete information");
            return utils.send(res);
        }
        const newProject = req.body;
        try {
            const newProjectToCreate = await ProjectService.createProject(newProject);
            if (!newProjectToCreate) {
                utils.setError(400, "The project with that name already exist or cannot found the organization");
            }
            else {
                utils.setSuccess(200, "Project created");
            }
            return utils.send(res);
        } catch (error) {
            utils.setError(404, error.message);
            return utils.send(res);
        }
    }

    static async getAllProjects (req, res) {
        const {page, pageSize} = req.params;
        if (!Number(page) || !Number(pageSize)) {
            utils.setError(400, "Please input valid number");
            return utils.send(res);
        }
        try {
            const allProjects = await ProjectService.getAllProjects(page, pageSize);
            if (allProjects.length > 0) {
                utils.setSuccess(200, "Project retrieved", allProjects);
            }
            else {
                utils.setSuccess(200, "Project not found");
            }
            return utils.send(res);
        } catch (error) {
            utils.setError(404, error.message);
            return utils.send(res);
        }
    }

    static async getProject (req, res) {
        const {id} = req.params;
        if (!Number(id)) {
            utils.setError(400, "Please input valid id");
            return utils.send(res);
        }
        try {
            const project = await ProjectService.getProject(Number(id));
            if (!project) {
                utils.setError(400, "Not found the project with that id");
            }
            else {
                utils.setSuccess(200, "Project found", project);
            }
            return utils.send(res);
        } catch (error) {
            utils.setError(404, error.message);
            return utils.send(res);
        }
    }

    static async updateProject (req, res) {
        const {id} = req.params;
        if (!Number(id)) {
            utils.setError(400, "Please input valid id");
            return utils.send(res);
        }
        const alterProject = req.body;
        try {
            const updateProject = await ProjectService.updateProject(id, alterProject);
            if (!updateProject) {
                utils.setError(400, "Cannot found the project with that id");
            }
            else {
                utils.setSuccess(200, "Project updated", updateProject);
            }
            return utils.send(res);
        } catch (error) {
            utils.setError(404, error.message);
            return utils.send(res);
        }
    }

    static async deleteProject (req, res) {
        const {id} = req.params;
        if (!Number(id)) {
            utils.setError(400, 'Please input valid id');
            return utils.send(res);
        }
        try {
            const deleteProject = await ProjectService.deleteProject(id);
            if (!deleteProject) {
                utils.setError(400, "Cannot found the project with that id");
            }
            else {
                utils.setSuccess(200, "Project deleted");
            }
            return utils.send(res);
        } catch (error) {
            utils.setError(404, error.message);
            return utils.send(res);
        }
    }
}

export default ProjectController;