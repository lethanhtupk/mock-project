import ServerService from '../services/ServerService';
import Util from '../utils/Utils';

const util = new Util();

class ServerController {

    static async getAllServers(req, res) {
        const {page, pageSize} = req.params;
        if (!Number(page) || !Number(pageSize)) {
            util.setError(400, "Please input valid number");
            return util.send(res);
        }
        try {
            const allServers = await ServerService.getAllServers(page, pageSize);
            if (allServers.length > 0) {
                util.setSuccess(200, 'Servers retrieved', allServers);
            } else {
                util.setSuccess(200, 'No server found');
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    }

    static async getServersBelongTo (req, res) {
        const {projectId, page, pageSize} = req.params;
        if (!Number(projectId) || !Number(page) || !Number(pageSize)) {
            util.setError(400, "Please input valid id");
            return util.send(res);
        }
        try {
            const servers = await ServerService.getServersBelongTo(projectId, page, pageSize);
            if (!servers) {
                util.setError(400, "Cannot found the project");
                return util.send(res);
            }
            else {
                if (servers.length > 0) {
                    util.setSuccess(200,"Server retrieved", servers);
                } else {
                    util.setSuccess(200,"This project dont have server right now");
                }
                return util.send(res);
            }   
        } catch (error) {
            util.setError(404, error.message);
            return util.send(res);
        }
    }

    static async addServer (req, res) {
        if (!req.body.name || !req.body.description || !req.body.avaiable) {
            util.setError("400", "Please provide complete details");
            return util.send(res);
        }
        const newServer = req.body;
        try {
            const createServer = await ServerService.addServer(newServer);
            if (!createServer) {
                util.setError(400, "The server's name already exist, please choose another")
            }
            else {
                util.setSuccess("200", "Server added!", createServer);
            }
            return util.send(res);
        } catch (error) {
           util.setError(400, error.message);
           return util.send(res);
        }
    }

    static async addServerToProject (req, res) {
        const {serverId, projectId} = req.params;
        if (!Number(serverId) || !Number(projectId)) {
            util.setError(400, "Plesae input valid id");
            return util.send(res);
        }
        try {
            const serverToAdd = await ServerService.addServerToProject(serverId, projectId)
            if (!serverToAdd) {
                util.setError(400, "Cannot find the server or project");
            }
            else {
                util.setSuccess(200, "Success add server to project");
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error.message);
            return util.send(res);
        }
    }

    static async addMultiServerToProject (req, res) {
        const {projectId} = req.params;
        if(!Number(projectId)) {
            util.setError(400, "Please input valid id");
            return util.send(res);
        }
        if(!req.body.serversId) {
            util.setError(400, "Please completed information");
            return util.send(res);
        }
        const serversId = req.body.serversId;
        try {
            for(let i = 0; i < serversId.length; i++) {
                const server = await ServerService.addServerToProject(serversId[i], projectId);
                if (!server) {
                    util.setError(404, "Cannot found the server or project or its already exist");
                    return util.send(res);
                }
            }
            util.setSuccess(200, "Servers added");
            return util.send(res);
        } catch (error) {
            util.setError(404, error.message);
            return util.send(res);
        }
    }

    static async updateServer (req, res) {
        const alteredServer = req.body;
        const {id} = req.params;
        if (!Number(id)){
            util.setError(400, "Please input valid numeric number");
            return util.send(res);
        }
        try {
            const updateServer = await ServerService.updateServer(id, alteredServer);
            if (!updateServer) {
                util.setError(404, "Cannot find the Server with the id ${id}");
            } 
            else {
                util.setSuccess(200, "Server updated", updateServer);
            }
            return util.send(res);          
        } catch (error) {
            util.setError(404, error);
            return util.send(res);
        }
    }

    static async getServer(req, res) {
        const {id} = req.params;

        if (!Number(id)){ 
            util.setError(404, "Please input valid id");
            return util.send(res);
        }
        try {
            const theServer = await ServerService.getServer(id);
            if (!theServer) {
                util.setError(404, "Cannot find the Server with the id ${id}");
            }
            else {
                util.setSuccess(200, "Found Server", theServer);
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error);
            return util.send(res);
        }
    }

    static async deleteServer(req, res) {
        const {id} = req.params;
        if (!Number(id)) {
            util.setError(400, "Please input valid id");
            return util.send(res);
        }
        try {
            const deleteServer = await ServerService.deleteServer(id);
            if (!deleteServer) {
                util.setError(404, "Cannot find the Server with id ${id}");
            }
            else {
                util.setSuccess(200, "Server deleted", deleteServer);
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error);
            return util.send(res);
        }
    }

    static async removeServer (req, res) {
        const {serverId, projectId}  = req.params;
        if (!Number(serverId) || !Number(projectId)) {
            util.setError(400, "Please input valid id");
            return util.send(res);
        }
        try {
            const removeServer = ServerService.removeServer(serverId, projectId);
            if (!removeServer) {
                util.setError(404, "Cannot found the server or project");
            }
            else {
                util.setSuccess("Server removed");
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error.message)
            return util.send(res);
        }
    }
}

export default ServerController;