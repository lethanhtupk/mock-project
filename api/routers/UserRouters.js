import Router from 'express';
import UserController from '../controllers/UserController';
import auth from '../middleware/auth';

const router = Router();

router.post('/register', UserController.register);
router.post('/login', UserController.login);
router.post('/logout', auth, UserController.logout);
router.post('/logoutAll', auth, UserController.logoutAll);

export default router;