import {Router} from 'express';
import OrganizationController from '../controllers/OrganizationController';
import auth from '../middleware/auth';

const router = Router();

router.get('/:page/:pageSize', auth, OrganizationController.getAllOrganizations);
router.get('/:id', auth, OrganizationController.getOrganization);
router.post('/create', auth, OrganizationController.addOrganization);
router.put('/update/:id', auth, OrganizationController.updateOrganization);
router.delete('/delete/:id', auth, OrganizationController.deleteOrganization);

export default router;