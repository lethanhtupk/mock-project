import {Router} from 'express';
import ServerController from '../controllers/ServerController';
import auth from '../middleware/auth';

const router = Router();

router.get('/:page/:pageSize',auth, ServerController.getAllServers);
router.get('/:id', auth, ServerController.getServer);
router.get('/projects/:projectId/:page/:pageSize', auth, ServerController.getServersBelongTo);
router.post('/create', auth, ServerController.addServer);
router.post('/:serverId/:projectId', auth, ServerController.addServerToProject);
router.post('/:projectId', ServerController.addMultiServerToProject);
router.put('/update/:id', auth, ServerController.updateServer);
router.delete('/delete/:id', auth, ServerController.deleteServer);
router.delete('/remove/:serverId/:projectId', auth, ServerController.removeServer);

export default router;