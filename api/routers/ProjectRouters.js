import {Router} from 'express';
import ProjectController from '../controllers/ProjectController';
import auth from '../middleware/auth';

const router = Router();

router.get('/:page/:pageSize', auth, ProjectController.getAllProjects);
router.post('/create', auth, ProjectController.createProject);
router.get('/:id', auth, ProjectController.getProject);
router.put('/update/:id', auth, ProjectController.updateProject);
router.delete('/delete/:id', auth, ProjectController.deleteProject);

export default router;
