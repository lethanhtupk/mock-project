import EmployeeController from '../controllers/EmployeeController';
import {Router} from 'express';
import auth from '../middleware/auth';

const router = Router();


router.get('/:page/:pageSize', auth, EmployeeController.getAllEmployees);
router.get('/:id', auth, EmployeeController.getEmployee);
router.get('/projects/:projectId/:page/:pageSize', auth, EmployeeController.getEmployeesBelongsTo);
// add mutiple employee to project 
router.post('/project/:projectId', EmployeeController.addMultiEmployeeToProject);
router.post('/create', auth, EmployeeController.createEmployee);
// add an employee to a project
router.post('/:employeeId/:projectId', auth, EmployeeController.addEmployeeToProject);
// create and add a employee to project
router.post('/:projectId', EmployeeController.createAndAddToProject);
router.put('/update/:id', auth, EmployeeController.updateEmployee);
router.delete('/delete/:id', auth, EmployeeController.deleteEmployee);
router.delete('/remove/:employeeId/:projectId', auth, EmployeeController.removeEmployee);
 
export default router;
