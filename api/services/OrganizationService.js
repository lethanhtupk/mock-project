import database from '../../models';

class OrganizationService {

    static async paginate(page, pageSize) {
        const offset = Number(page) * Number(pageSize);
        const limit = offset + Number(pageSize);
        return {
            offset,
            limit
        };
    }

    static async createOrganization (newOrganization) {
        try {
            const organizationToCreate = await database.Organization.findOne({
                where: {name: newOrganization.name}
            });
            if (!organizationToCreate) {
                return await database.Organization.create(newOrganization);
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    static async getAllOrganizations (page, pageSize) {
        try {
            const paginate = await this.paginate(page, pageSize);
            return await database.Organization.findAll({
                offset: paginate.offset,
                limit: paginate.limit
            });
        } catch (error) {
            throw error;
        }
    }

    static async getOrganization(id) {
        try {
            const theOrganization = await database.Organization.findByPk(Number(id));
            return theOrganization;
        } catch (error) {
            throw error;
        }
    }

    static async updateOrganization (id, updateOrganization) {
        try {
            const organizationToUpdate = await database.Organization.findByPk(Number(id));
            if (organizationToUpdate) {
                await database.Organization.update(updateOrganization, {where: {id: Number(id)}});
                return updateOrganization;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    static async deleteOrganization (id) {
        try {
            const organizationToDelete = await database.Organization.findByPk(Number(id));
            if (organizationToDelete) {
                const deleteOrganization = await database.Organization.destroy({
                    where: {
                        id: Number(id)
                    }
                });
                return deleteOrganization;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }
}

export default OrganizationService;