require('dotenv').config();
import database from '../../models';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import passportJWT from 'passport-jwt';
import { totalmem } from 'os';

const ExtractJwt = passportJWT.ExtractJwt;
const jwtOptions = {};
// jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
jwtOptions.secretOrKey = process.env.SECRETKEY;


class UserService {
    static async createUser (newUser) {
        try {
            const newUserToAdd = await database.User.findOne({
                where: {username: newUser.username}
            });
            if (!newUserToAdd) {
                bcrypt.hash(newUser.password, 10, function (error, hash) {
                    if (error) {
                        throw error;
                    }
                    newUser.password = hash;
                    database.User.create(newUser);
                });
                return newUser;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    static async loginUser (user) {
        try {
            const userToLogin = await database.User.findOne({
                where: {username: user.username}
            });
            if (userToLogin) {
                const isMatch = await bcrypt.compare(user.password, userToLogin.password);
                if (isMatch) {
                    const payload = {id: userToLogin.id};
                    const token = jwt.sign(payload, jwtOptions.secretOrKey);
                    await database.Token.create({token: token, userId: userToLogin.id});
                    return token;
                }
                return null;
            }
            else {
                return null;
            }
        } catch (error) {
            throw error;
        }
    }

    static async logout (token) {
        try {
            await database.Token.destroy({
                where: {token: token}
            })
        } catch (error) {
            throw error;
        }
    }
    
    static async logoutAll (user) {
        try {
            await database.Token.destroy({
                where: {userId: user.id}
            });
        } catch (error) {
            throw error;
        }
    }
}

export default UserService;