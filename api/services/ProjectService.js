import database from '../../models';

class ProjectService {

    static async paginate(page, pageSize) {
        const offset = Number(page) * Number(pageSize);
        const limit = offset + Number(pageSize);
        return {
            offset,
            limit
        };
    }
    
    static async createProject(newProject) {
        try {
            const organization = await database.Organization.findOne({
                where: {id: Number(newProject.organizationId)}
            });
            const newProjectToAdd = await database.Project.findOne({
                where: {name: newProject.name}
            });
            if(!newProjectToAdd && organization){
                await database.Project.create(newProject);
                return newProject;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    static async getAllProjects(page, pageSize) {
        try {
            const paginate = await this.paginate(page, pageSize);
            return await database.Project.findAll({
                offset: paginate.offset,
                limit: paginate.limit
            });
        } catch (error) {
            throw error;
        }
    }

    static async getProject (id) {
        try {
            const theProject = await database.Project.findByPk(Number(id));
            return theProject;
        } catch (error) {
            throw error;
        }
    }

    static async updateProject (id, updateProject) {
        try {
            const projectToUpdate = await database.Project.findByPk(Number(id));
            if (projectToUpdate) {
                await database.Project.update(updateProject, {where: {id: Number(id)}});
                return updateProject;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    static async deleteProject (id) {
        try {
            const projectToDelete = await database.Project.findByPk(Number(id));
            if (projectToDelete) {
                const projectDelete = await database.Project.destroy({
                    where: {id: id}
                });
                return projectToDelete;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }
}

export default ProjectService;