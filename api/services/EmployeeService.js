import database from '../../models';

class EmployeeService {

    static async paginate(page, pageSize) {
        const offset = Number(page) * Number(pageSize);
        const limit = offset + Number(pageSize);
        return {
            offset,
            limit
        };
    }

    static async getAllEmployees (page, pageSize) {
        try {
            const paginate = await this.paginate(page, pageSize);
            return await database.Employee.findAll({
                offset: paginate.offset,
                limit: paginate.limit
            });
        } catch (error) {
            throw error;
        }
    }

    static async getEmployeeBelongsTo (projectId, page, pageSize) {
        try {
            const project = await database.Project.findByPk(Number(projectId));
            if (project) {
                const paginate = await this.paginate(page, pageSize);
                return await database.EmployeeProject.findAll({
                    where: {
                        project_id: Number(projectId)
                    },
                    offset: paginate.offset,
                    limit: paginate.limit,
                    include: [{
                        model: database.Employee,
                    }]
                });
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    static async getEmployee(id) {
        try {
            return await database.Employee.findByPk(Number(id));
        } catch(error) {
            throw error;
        }
    }

    static async addEmployee(newEmployee) {
        try {
            const employeeToadd = await database.Employee.findOne({
                where: {name: newEmployee.name}
            });
            if (!employeeToadd) {
                return await database.Employee.create(newEmployee);
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    // add employee to a project
    static async addEmployeeToProject (employeeId, projectId, role) {
        try {
            const employee = await database.Employee.findByPk(Number(employeeId));
            const project = await database.Project.findByPk(Number(projectId));
            if (employee && project) {
                const employeeProject = await database.EmployeeProject.findOne({
                    where: {
                        employee_id: employeeId,
                        project_id: projectId
                    }
                });
                if (employeeProject) {
                    return null;
                }
                else {
                    return await database.EmployeeProject.create({
                        employee_id: employeeId,
                        project_id: projectId,
                        role: role
                    });
                }   
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    static async updateEmployee (id, updateEmployee) {
        try {
            const employeeToUpdate = await database.Employee.findByPk(Number(id));
            if (employeeToUpdate) {
                await database.Employee.update(updateEmployee, {where: {id: Number(id)}});
                return updateEmployee
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    static async deleteEmployee (id) {
        try {
            const employeeToDelete = await database.Employee.findByPk(Number(id));
            if (employeeToDelete) {
                const employeeDelete = await database.Employee.destroy({
                    where: {id: Number(id)}
                });
                return employeeToDelete;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    static async removeEmployee (employeeId, projectId) {
        try {
            const removeEmployee = await database.EmployeeProject.findOne({
                where: {
                    employee_id: Number(employeeId),
                    project_id: Number(projectId)
                }
            })
            if (removeEmployee) {
                await database.EmployeeProject.destroy({
                    where: {
                        employee_id: Number(employeeId),
                        project_id: Number(projectId)
                    }
                })
                return removeEmployee;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }
}

export default EmployeeService;