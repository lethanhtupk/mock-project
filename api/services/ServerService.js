import database from '../../models';

class ServerService {

    static async paginate(page, pageSize) {
        const offset = Number(page) * Number(pageSize);
        const limit = offset + Number(pageSize);
        return {
            offset,
            limit
        };
    }

    static async getAllServers(page, pageSize) {
        try {
            const paginate = await this.paginate(page, pageSize);
            return await database.Server.findAll({
                offset: paginate.offset,
                limit: paginate.limit
            });
        } catch (error) {
            throw error;
        }
    }

    static async getServersBelongTo (projectId, page, pageSize) {
        try {
            const project = await database.Project.findByPk(Number(projectId));
            if (project) {
                const paginate = await this.paginate(page, pageSize);
                const servers = await database.ProjectServer.findAll({
                    where: {
                        project_id: Number(projectId)
                    },
                    offset: paginate.offset,
                    limit: paginate.limit,
                    include: [{
                        model: database.Server,
                    }]
                });
                return servers;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    static async addServer(newServer) {
        try {
            const newServerToAdd = await database.Server.findOne({
                where: {name: newServer.name}
            })
            if (!newServerToAdd) {
                return await database.Server.create(newServer);
            }
            return null
        } catch (error) {
            throw error;
        }
    }

    // add a server to a project
    static async addServerToProject(serverId, projectId) {
        try {
            const serverToAdd = await database.Server.findByPk(Number(serverId));
            const projectToAdd = await database.Project.findByPk(Number(projectId));
            if (serverToAdd && projectToAdd) {
                const serverProject = await database.ProjectServer.findOne({
                    where: {server_id: serverId, project_id: projectId}
                });
                if (serverProject) {
                    return null;
                }
                return await database.ProjectServer.create({
                    server_id: serverId,
                    project_id: projectId
                });
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    static async updateServer(id, updateServer) {
        try {
            const serverToUpdate = await database.Server.findByPk(Number(id));
            if (serverToUpdate) {
                await database.Server.update(updateServer, {where: {id: Number(id)}});
                return updateServer;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    static async getServer(id) {
        try {
            const theServer =  await database.Server.findOne({
                where: {id: Number(id)}
            })
            return theServer;
        } catch (error) {
            throw error;
        }
    }

    static async deleteServer(id) {
        try {
            const ServerToDelete = database.Server.findOne({
                where: {id: Number(id)}
            });
            if (ServerToDelete) {
                const deleteServer = await database.Server.destroy({
                    where: {id: Number(id)}
                });
                return deleteServer;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    static async removeServer (serverId, projectId) {
        try {
            const removeServer = await database.ProjectServer.findOne({
                where: {
                    server_id: serverId,
                    project_id: projectId
                }
            });
            if (removeServer) {
                await database.ProjectServer.destroy({
                    where: {
                        server_id: serverId,
                        project_id: projectId
                    }
                });
                return removeServer;
            }
            return null;
        } catch (error) {
            throw error;
        }
    } 
}

export default ServerService;