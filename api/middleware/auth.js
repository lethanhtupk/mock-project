const jwt = require('jsonwebtoken');
import database from '../../models';
import Util from '../utils/Utils';

const utils = new Util();

const auth  = async(req, res, next) => {
    if (!req.header('Authorization')) {
        utils.setError(400, "Please provide jwt in header");
        return utils.send(res);
    }
    const token = req.header('Authorization').replace('Bearer ', '');
    const data = jwt.verify(token, process.env.SECRETKEY);
    try {
        const user = await database.User.findOne({
            where: {id: data.id}
        });
        if (user) {
            const tokenInDB = await database.Token.findOne({
                where: {userId: data.id, token: token}
            });
            if (!tokenInDB) {
                utils.setError(401, "Not authorized to access this resource");
                return utils.send(res);
            }
            req.token = tokenInDB;
            req.user = user;
            next();
        }
        else {
            utils.setError(404, "Cannot found the user");
            return utils.send(res);
        }
    } catch (error) {
        utils.setError(400, error.message);
        return utils.send(res);
    }
}

export default auth;