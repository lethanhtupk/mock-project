'use strict';
module.exports = (sequelize, DataTypes) => {
  const ProjectServer = sequelize.define('ProjectServer', {
    project_id: DataTypes.INTEGER,
    server_id: DataTypes.INTEGER,
    usage: DataTypes.TEXT
  }, {});
  ProjectServer.associate = function(models) {
    // associations can be defined here
    ProjectServer.belongsTo(models.Server, {
      foreignKey: 'server_id',
      onDelete: 'cascade'
    });
    ProjectServer.belongsTo(models.Project, {
      foreignKey: 'project_id', 
      onDelete: 'cascade'
    })

  };
  return ProjectServer;
};