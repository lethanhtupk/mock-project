'use strict';
module.exports = (sequelize, DataTypes) => {
  const Project = sequelize.define('Project', {
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    creator: DataTypes.STRING,
    organizationId: DataTypes.INTEGER
  }, {});
  Project.associate = function(models) {
    // associations can be defined here
    Project.belongsTo(models.Organization, {foreignKey: 'organizationId', as: 'organization'});
    Project.belongsToMany(models.Server, {through: 'ProjectServer', foreignKey: 'project_id', as: 'servers'})
    Project.belongsToMany(models.Employee, {
      through: 'EmployeeProject',
      foreignKey: 'project_id',
      as: 'employees'
    })
  };
  return Project;
};