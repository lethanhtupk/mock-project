'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmployeeProject = sequelize.define('EmployeeProject', {
    employee_id: DataTypes.INTEGER,
    project_id: DataTypes.INTEGER,
    role: DataTypes.TEXT
  }, {});
  EmployeeProject.associate = function(models) {
    // associations can be defined here
    EmployeeProject.belongsTo(models.Employee, {
      foreignKey: 'employee_id',
      onDelete: 'cascade'
    });
    EmployeeProject.belongsTo(models.Project, {
      foreignKey: 'project_id',
      onDelete: 'cascade'
    })
    };
  return EmployeeProject;
};