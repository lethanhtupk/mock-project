'use strict';
module.exports = (sequelize, DataTypes) => {
  const Server = sequelize.define('Server', {
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    avaiable: DataTypes.BOOLEAN
  }, {});

  Server.associate = function(models) {
    // associations can be defined here
    Server.belongsToMany(models.Project, {
      through: 'ProjectServer',
      foreignKey: 'server_id',
      as: 'projects'
    })
  };
  
  return Server;
};
