'use strict';
module.exports = (sequelize, DataTypes) => {
  const Employee = sequelize.define('Employee', {
    name: DataTypes.STRING,
    birthday: DataTypes.DATE,
    email: DataTypes.STRING,
    title: DataTypes.STRING,
    phone_number: DataTypes.STRING
  }, {});
  Employee.associate = function(models) {
    // associations can be defined here
    Employee.belongsToMany(models.Project, {
      through: 'EmployeeProject',
      foreignKey: 'employee_id',
      as: 'projects'
    })
  };
  return Employee;
};