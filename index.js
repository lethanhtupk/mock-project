import express from 'express';
import bodyParser from 'body-parser';

// import router
import serverRouters from './api/routers/ServerRouters';
import userRouters from './api/routers/UserRouters';
import projectRouters from './api/routers/ProjectRouters';
import employeeRouters from './api/routers/EmployeeRouters';
import organizationRouters from './api/routers/OrganizationRouters';



const app = express();


// getting data from header and body of request
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

// port for application
const port = process.env.PORT || 8080;

// setting router
app.use('/api/servers', serverRouters);
app.use('/api/user/',userRouters);
app.use('/api/projects/', projectRouters);
app.use('/api/employees/', employeeRouters);
app.use('/api/organizations', organizationRouters);

// when a random route is inputed
app.get('*', (req, res) => res.status(200).send({
    message: 'Welcome to this API.'
}));

app.listen(port, () => {
    console.log(`Server is running on PORT ${port}`);
});

export default app;